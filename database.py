from sqlalchemy import MetaData
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

database_url = "sqlite+aiosqlite:///database.db"
engine = create_async_engine(
    database_url, echo=True, connect_args={"check_same_thread": False}
)

async_session = sessionmaker(engine, class_=AsyncSession)
session = async_session()
Base = declarative_base(metadata=MetaData())
