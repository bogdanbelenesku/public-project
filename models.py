from sqlalchemy import Column
from sqlalchemy.types import Integer, String

from database import Base


class Recipe(Base):
    __tablename__ = "recipes"

    id = Column(Integer(), primary_key=True)
    name = Column(String(50), nullable=False)
    minutes_to_cook = Column(Integer(), nullable=False)
    ingredient_list = Column(String(100), nullable=False)
    description = Column(String(100), nullable=True)
    view_count = Column(Integer(), default=0)
