from typing import Dict, Tuple

from fastapi import FastAPI
from sqlalchemy import select, update

from database import engine, session
from models import Base, Recipe

app = FastAPI()


@app.on_event("startup")
async def startup():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)


@app.on_event("shutdown")
async def shutdown():
    await engine.dispose()


@app.get("/recipes")
async def recipes_list(limit: int = 10):
    recipe_fields = [Recipe.name, Recipe.view_count, Recipe.minutes_to_cook]
    output = await session.execute(select(*recipe_fields).limit(limit))
    recipes_query = output.fetchall()
    if recipes_query:
        return recipes_query, 202
    return {"message": "No content"}, 203


@app.get("/recipes/detail")
async def recipe_detail(name: str) -> Tuple[Dict, int]:
    sql_script = select(Recipe).where(Recipe.name.like(name))
    recipes = await session.execute(sql_script)
    recipe_query = recipes.fetchone()
    if recipe_query:
        recipe: Recipe = recipe_query[0]
        output_count = await session.execute(
            select(Recipe.view_count).filter_by(id=recipe.id)
        )
        current_count_query = output_count.fetchone()
        if current_count_query:
            current_count: int = current_count_query[0]
            await session.execute(
                update(Recipe)
                .filter_by(id=recipe.id)
                .values({"view_count": current_count + 1})
            )
        await session.commit()
        fields = ["name", "minutes_to_cook", "ingredient_list", "description"]
        return {key: getattr(recipe, key) for key in fields}, 202
    return {"message": "No content"}, 203
